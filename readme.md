# Truenas ACME Authenticator For Epik

NOTE: I do not use this script anymore so I don't know if it still works.

This is a simple script for truenas to edit DNS records of [epik.com](https://epik.com) domains. It's intended use is with the ACME DNS-Authenticator option in truenas using the **shell** option.

## Installation

Run the following in the Truenas shell

- `git clone https://gitlab.com/pixqw/taafe && cd taafe`
- `echo "EPIK_SIGNATURE=<your api key here>" > token`

In the web interface navigate to **Credentials** > **Certificates** > **ACME DNS-Authenticators** and select **Add**. The **Authenticator** should be set to **shell** and **Authenticator script** should be set to the `acme_auth.sh` script.

## Additional resources

- <https://www.truenas.com/community/threads/howto-acme-dns-authenticator-shell-script-using-acmesh-project.107252/>
