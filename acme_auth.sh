#!/bin/sh

set -ex

# https://registrar.epik.com/account/api-settings/
# shellcheck source=/dev/null
. "$(dirname "$0")"/token

LOGFILE=$(dirname "$0")/acme.log

add_record() {
    # Only take the domain.tld and never any subdomains
    domain=$(echo "$1" | awk -F"." '{print $(NF-1) FS $NF}')
    subdomain=$2
    txtvalue=$3

    # FIXME: Records added this way are always surrounded by a "", not sure if this is a problem
    curl \
        -X POST "https://usersapiv2.epik.com/v2/domains/$domain/records?SIGNATURE=$EPIK_SIGNATURE" \
        -H "accept: application/json" -H "Content-Type: application/json" \
        -d "{
            \"create_host_records_payload\": {
                \"HOST\": \"$subdomain\", 
                \"TYPE\": \"TXT\", 
                \"DATA\": \"$txtvalue\", 
                \"AUX\": 0, 
                \"TTL\": 10
            }
        }" | jq
}

del_record() {
    domain=$(echo "$1" | awk -F"." '{print $(NF-1) FS $NF}')
    subdomain=$2
    txtvalue=$3
    
    # The delete option requires the id of the record
    record_id=$(curl \
        -X GET "https://usersapiv2.epik.com/v2/domains/$domain/records?SIGNATURE=$EPIK_SIGNATURE" \
        -H "accept: application/json" \
    | jq ".data.records[] | select(.data == \"\\\"$txtvalue\\\"\") | .id" \
    | tr -d '"')

    curl \
        -X DELETE "https://usersapiv2.epik.com/v2/domains/$domain/records?SIGNATURE=$EPIK_SIGNATURE&ID=$record_id" \
        -H "accept: application/json" | jq
}

log2file() {
    echo "$(date "+[%a %b %d %H:%M:%S %Z %Y]"): $*" | tee -a "$LOGFILE"
}

log2file "./$(basename "$0") $*"

if [ "$1" = "set" ]; then
    add_record "$2" "$3" "$4"
elif [ "$1" = "unset" ]; then
    del_record "$2" "$3" "$4"
else
    log2file "Unknown command $1, see \`$LOGFILE\`"
fi